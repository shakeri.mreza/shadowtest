package shakeri.mamzi.test;
//created by mamzi at 10/12/18 

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class SimpleAdapter extends PagerAdapter {


    private Context context;
    private ArrayList<Integer> colors;


    SimpleAdapter(Context context, ArrayList<Integer> colors) {
        this.context = context;
        this.colors = colors;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup collection, int position) {
        final int color = colors.get(position);
        final ViewGroup layout = (ViewGroup) LayoutInflater
                .from(context)
                .inflate(R.layout.simple_back, collection, false);
        final FrameLayout back = layout.findViewById(R.id.back);
        final TextView text = layout.findViewById(R.id.textView);
        text.setText("Number " + (position + 1));
        back.setBackgroundColor(color);
        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup collection, int position, @NonNull Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return colors.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return String.valueOf(colors.get(position));
    }

}
