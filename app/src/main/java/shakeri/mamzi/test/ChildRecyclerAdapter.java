package shakeri.mamzi.test;
//created by mamzi at 10/12/18 

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class ChildRecyclerAdapter extends RecyclerView.Adapter<ChildRecyclerAdapter.CoreViewHolder> {

    private static final int PAGER = 0;
    private static final int ROUNDED = 1;
    private static final int TALL = 2;
    private int type;




    @NonNull
    @Override
    public CoreViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if (i == ROUNDED) {
            return new ChildRecyclerAdapter.Rounded(LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.design_rounded, viewGroup, false)
            );
        } else {
            return new ChildRecyclerAdapter.Tall(LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.design_tall, viewGroup, false)
            );
        }
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull CoreViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.itemView.clearAnimation();

    }

    int lastPosition;
    @Override
    public void onBindViewHolder(@NonNull CoreViewHolder coreViewHolder, int i) {
        coreViewHolder.bind(i);
        if (i == 9) {
            Animation animation = AnimationUtils.loadAnimation(coreViewHolder.itemView.getContext(),
                    (R.anim.show));
            coreViewHolder.next.startAnimation(animation);
            lastPosition = i;
        }
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    @Override
    public int getItemViewType(int position) {
        return type;
    }

    public void setType(int typeByPos) {
        this.type = typeByPos;
    }


    class Rounded extends CoreViewHolder {

        Rounded(@NonNull View itemView) {
            super(itemView);
        }
    }

    class Tall extends CoreViewHolder {
        protected TextView detail;

        Tall(@NonNull View itemView) {
            super(itemView);
            detail = itemView.findViewById(R.id.textView6);
        }


        @Override
        void bind(int pos) {
            super.bind(pos);
            if (pos == 0) {

                detail.setVisibility(View.VISIBLE);
            } else if (pos == 9) {
                detail.setVisibility(View.INVISIBLE);


            } else {

                detail.setVisibility(View.VISIBLE);

            }
        }
    }

    class CoreViewHolder extends RecyclerView.ViewHolder {
        protected TextView name;
        private ImageView image;
        protected TextView bigTitle;
        protected ImageView arrow;
        protected ConstraintLayout parent;

        FrameLayout next;

        CoreViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            image = itemView.findViewById(R.id.image);
            bigTitle = itemView.findViewById(R.id.big_title);
            arrow = itemView.findViewById(R.id.arrow);
            parent = itemView.findViewById(R.id.parent);
            next = itemView.findViewById(R.id.next);
        }

        void bind(int pos) {
            name.setText("Item " + (pos + 1));
            image.setImageResource(R.color.four);
            bigTitle.setText("Title here");


            if (pos == 0) {
                parent.setBackgroundResource(R.drawable.start);

                next.setVisibility(View.INVISIBLE);
                name.setVisibility(View.VISIBLE);
                image.setVisibility(View.VISIBLE);
                bigTitle.setVisibility(View.VISIBLE);
                arrow.setVisibility(View.VISIBLE);


            } else if (pos == 9) {
                parent.setBackgroundResource(R.drawable.end);
                next.setVisibility(View.VISIBLE);
                name.setVisibility(View.INVISIBLE);
                image.setVisibility(View.INVISIBLE);
                bigTitle.setVisibility(View.INVISIBLE);
                arrow.setVisibility(View.INVISIBLE);

            } else {
                parent.setBackgroundResource(R.drawable.middle);
                next.setVisibility(View.INVISIBLE);
                name.setVisibility(View.VISIBLE);
                image.setVisibility(View.VISIBLE);
                bigTitle.setVisibility(View.INVISIBLE);
                arrow.setVisibility(View.INVISIBLE);

            }
        }

    }


}
