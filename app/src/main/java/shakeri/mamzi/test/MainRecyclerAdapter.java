package shakeri.mamzi.test;
//created by mamzi at 10/12/18 

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if (i == 0) {

            return new PagerViewHolder(LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.design_pager, viewGroup, false)
            );
        } else {
            return new RecyclerViewHolder(LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.design_recycler_horizontal_item, viewGroup, false)
            );
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof RecyclerViewHolder) {
            ((RecyclerViewHolder)viewHolder).bind(i);
        }

    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? 0 : 1;
    }


    @Override
    public int getItemCount() {
        return 6;
    }

    class RecyclerViewHolder extends RecyclerView.ViewHolder {

        ChildRecyclerAdapter adapter;

        RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);

            final RecyclerView recyclerView = itemView.findViewById(R.id.rec);
            recyclerView.setHasFixedSize(true);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            final LinearLayoutManager layoutManager = new LinearLayoutManager(itemView.getContext());
            layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            recyclerView.setLayoutManager(layoutManager);
            adapter = new ChildRecyclerAdapter();
            recyclerView.setAdapter(adapter);
        }

        private void bind(int pos) {

            int typeByPos;
            switch (pos) {
                case 0:
                    typeByPos = 0;
                    break;
                case 1:
                    typeByPos = 1;
                    break;
                default:
                    typeByPos = 2;
                    break;
            }
            adapter.setType(typeByPos);
        }
    }


    class PagerViewHolder extends RecyclerView.ViewHolder {
        private List<ImageView> dots;
        private Timer timer;

        PagerViewHolder(@NonNull View itemView) {
            super(itemView);
            final ViewPager viewPager = itemView.findViewById(R.id.view_pager);
            ArrayList<Integer> list = new ArrayList<>();
            list.add(Color.parseColor("#e0999f"));
            list.add(Color.parseColor("#66023c"));
            viewPager.setAdapter(new SimpleAdapter(itemView.getContext(), list));
            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int i, float v, int i1) {

                }

                @Override
                public void onPageSelected(int i) {
                    selectDot(i);
                }

                @Override
                public void onPageScrollStateChanged(int i) {

                }
            });

            addDots();
            selectDot(0);
            setTimer(viewPager);
        }


        private void setTimer(final ViewPager viewPager) {
            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    viewPager.post(new Runnable() {
                        @Override
                        public void run() {
                            viewPager.setCurrentItem((viewPager.getCurrentItem() + 1) % 2);
                        }
                    });
                }
            };
            timer = new Timer();
            timer.schedule(timerTask, 2000, 3000);
        }

        private void addDots() {
            dots = new ArrayList<>();
            LinearLayout dotsLayout = itemView.findViewById(R.id.dots);

            for (int i = 0; i < 2; i++) {
                ImageView dot = new ImageView(itemView.getContext());
                dot.setBackgroundResource(R.drawable.pager_dot_not_selected);

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        22,
                        22
                );
                dotsLayout.addView(dot, params);
                params.setMargins(0, 0, 24, 0);
                dots.add(dot);
            }
        }


        private void selectDot(int position) {
            for (int i = 0; i < 2; i++) {
                int drawableId = (i == position)
                        ? (R.drawable.pager_dot_selected)
                        : (R.drawable.pager_dot_not_selected);
                dots.get(i).setBackgroundResource(drawableId);
            }
        }
    }
}
