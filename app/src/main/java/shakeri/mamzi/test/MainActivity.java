package shakeri.mamzi.test;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yarolegovich.slidingrootnav.SlideGravity;
import com.yarolegovich.slidingrootnav.SlidingRootNav;
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder;
import com.yarolegovich.slidingrootnav.callback.DragListener;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        SlidingRootNav slidingRootNav = new SlidingRootNavBuilder(this)
                .withMenuLayout(R.layout.design_drawer_menu)
                .withGravity(SlideGravity.LEFT)
                .withDragDistance(240)
                .withToolbarMenuToggle(toolbar)
                .addDragListener(new DragListener() {
                    @Override
                    public void onDrag(float progress) {
                        Log.w("=====", progress + "");
                    }
                })
                .inject();


        final LinearLayout buttons = findViewById(R.id.buttons);

        for (int i = 0; i < buttons.getChildCount(); i++) {
            final TextView t = (TextView) buttons.getChildAt(i);
            t.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    for (int i = 0; i < buttons.getChildCount(); i++) {
                        final TextView t = (TextView) buttons.getChildAt(i);
                        t.setBackgroundResource(R.drawable.selected_not);
                    }

                    t.setBackgroundResource(R.drawable.selected);
                }
            });
        }


        final RecyclerView recyclerView = findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        MainRecyclerAdapter adapter = new MainRecyclerAdapter();
        recyclerView.setAdapter(adapter);

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
